package imperative;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static imperative.Main.Gender.FEMALE;
import static imperative.Main.Gender.MALE;

public class Main {
    public static void main(String[] args) {
        // create a List of Person
        List<Person> people = List.of(
                new Person("JoJo", MALE),
                new Person("LiLi", FEMALE),
                new Person("David", MALE),
                new Person("Marie-Jo", FEMALE),
                new Person("Dukenzo", MALE),
                new Person("Flo", FEMALE),
                new Person("MiMi", FEMALE)
        );

        System.out.println("// imperative approach");
        List<Person> females = new ArrayList<>(); // this list should store only females
        for (Person person : people) {
            if (FEMALE.equals(person.gender)){
                females.add(person);
            }
        }

        for (Person female : females) {
            System.out.println(female);
        }

        // declarative approach
        System.out.println("\n// declarative approach");
        people.stream()
                .filter(person -> FEMALE.equals(person.gender))
                .forEach(System.out::println);
    }

    // local class
    static class Person{
        private final String name;
        private final Gender gender;

        Person(String name, Gender gender){
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    enum Gender{
        MALE, FEMALE
    }
}
