package streams;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static streams.Stream.Gender.FEMALE;
import static streams.Stream.Gender.MALE;


public class Stream {
    public static void main(String[] args) {
        // create a List of Person
        List<Person> people = List.of(
                new Person("JoJo", MALE),
                new Person("LiLi", FEMALE),
                new Person("David", MALE),
                new Person("Marie-Jo", FEMALE),
                new Person("Dukenzo", MALE),
                new Person("Flo", FEMALE),
                new Person("MiMi", FEMALE)
        );

        final Function<Person, String> personStringFunction = person -> person.name;
        people.stream()
                .map(personStringFunction)
                .mapToInt(String::length)
                .forEach(System.out::println);

    }

    // local class
    static class Person{
        private final String name;
        private final Gender gender;

        Person(String name, Gender gender){
            this.name = name;
            this.gender = gender;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", gender=" + gender +
                    '}';
        }
    }

    enum Gender{
        MALE, FEMALE
    }
}
