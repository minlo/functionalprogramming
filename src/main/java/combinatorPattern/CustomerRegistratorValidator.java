package combinatorPattern;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

import static combinatorPattern.CustomerRegistratorValidator.ValidationResult.*;

public interface CustomerRegistratorValidator
        extends Function<Customer, CustomerRegistratorValidator.ValidationResult> {

    static CustomerRegistratorValidator isEmailValid(){
        return customer -> customer.getEmail().contains("@") ?
                SUCCESS : EMAIL_NOT_VALID;
    }

    static CustomerRegistratorValidator isPhoneNumberValid(){
        return customer -> customer.getPhoneNumber().startsWith("6") && customer.getPhoneNumber().length() == 9 ?
                SUCCESS : PHONE_NUMBER_NOT_VALID;
    }

    static CustomerRegistratorValidator isAdult(){
        return customer -> Period.between(LocalDate.now(), customer.getDob()).getYears() >= 18 ?
                 SUCCESS :  IS_NOT_ADULT;
    }

    default CustomerRegistratorValidator and (CustomerRegistratorValidator other){
        return customer -> {
            final ValidationResult result = this.apply(customer);
            return result.equals(SUCCESS) ? other.apply(customer) : result;
        };



    }

    enum ValidationResult{
        SUCCESS,
        PHONE_NUMBER_NOT_VALID,
        EMAIL_NOT_VALID,
        IS_NOT_ADULT
    }

}
