package combinatorPattern;

import java.time.LocalDate;
import java.time.Period;

public class CustomerValidatorService {

    // Simple Email Validation
    private boolean isEmailValid(String email){
        return email.contains("@");
    }

    // Simple PhoneNumber Validation
    private boolean isPhoneNumberValid(String phoneNumber){
        return phoneNumber.startsWith("6") && phoneNumber.length() == 9;
    }

    // Simple Date of birth Validation
    private boolean isAdult(LocalDate dob){
        return Period.between(dob, LocalDate.now()).getYears() >= 18;
    }

    // Is Customer valid?
    public boolean isValid(Customer customer){
        return isAdult(customer.getDob()) &&
                isEmailValid(customer.getEmail()) &&
                isPhoneNumberValid(customer.getPhoneNumber());
    }


}
