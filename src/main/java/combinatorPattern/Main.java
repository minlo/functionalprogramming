package combinatorPattern;

import java.time.LocalDate;

import static combinatorPattern.CustomerRegistratorValidator.ValidationResult.SUCCESS;

public class Main {
    public static void main(String[] args) {
        Customer customer = new Customer(
                "JoJo",
                "jojo@dotcomfly.com",
                "69889663",
                LocalDate.of(2000,6,18));

        // Using normal Validator logic
        CustomerValidatorService validatorService = new CustomerValidatorService();
        final boolean isValid = validatorService.isValid(customer);
        System.out.println(isValid);

        // Using combinatorPattern
        final CustomerRegistratorValidator.ValidationResult result = CustomerRegistratorValidator.isEmailValid()
                .and(CustomerRegistratorValidator.isPhoneNumberValid())
                .and(CustomerRegistratorValidator.isAdult())
                .apply(customer);
        //System.out.println(result);
        if (result != SUCCESS){
            throw new IllegalStateException(result.name());
        }
    }
}
