package optional;

import java.util.Optional;

public class _Optional {
    public static void main(String[] args) {
         Optional.ofNullable("joel@dotcomlfy.com")
                .ifPresent(email -> System.out.println("sending email to " + email));// if present

        Optional.ofNullable(null)
                .ifPresentOrElse(email2 -> System.out.println("sending email to " + email2),
                        () -> System.out.println("cannot send email"));
    }
}
