package functionnalInterface;

import java.util.function.Predicate;

public class _Predicate {
    public static void main(String[] args) {
        System.out.println("// Without Predicate");
        System.out.println(isPhoneNumberValid("698896698"));
        System.out.println(isPhoneNumberValid("798896698"));
        System.out.println(isPhoneNumberValid("69889669"));

        System.out.println("\n// With Predicate");
        System.out.println(isPhoneNumberValidPredicate.test("698896698"));
        System.out.println(isPhoneNumberValidPredicate.test("798896698"));
        System.out.println(isPhoneNumberValidPredicate.test("69889669"));

        System.out.println("\n// With Predicate und contains number 3");
        System.out.println(phoneContainsNumber3.test("698836698"));
        System.out.println(phoneContainsNumber3.test("79889698"));
        System.out.println(phoneContainsNumber3.test("69883669"));

        System.out.println("\n// is PhoneNumber Valid and Contains number 3 ?");
        System.out.println(isPhoneNumberValidPredicate.and(phoneContainsNumber3).test("698836698"));
        System.out.println(isPhoneNumberValidPredicate.and(phoneContainsNumber3).test("79889698"));
        System.out.println(isPhoneNumberValidPredicate.and(phoneContainsNumber3).test("69883669"));



    }

    static boolean isPhoneNumberValid(String phoneNumber){
        return phoneNumber.startsWith("6") && phoneNumber.length() == 9;
    }

    static Predicate<String> isPhoneNumberValidPredicate =
            phoneNumber -> phoneNumber.startsWith("6") && phoneNumber.length() == 9;

    static Predicate<String> phoneContainsNumber3 = phoneNumber -> phoneNumber.contains("3");
}
