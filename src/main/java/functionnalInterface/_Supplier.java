package functionnalInterface;

import java.util.List;
import java.util.function.Supplier;

public class _Supplier {
    public static void main(String[] args) {
        System.out.println("Without Supplier \n"+getDBConnectUrl());
        System.out.println("\nWith Supplier \n"+dBConnectUrl.get());
        System.out.println("\nWith Supplier \n"+dBConnectUrlList.get());
    }

    static String getDBConnectUrl(){
        return "jdbc://localhost:3036/db";
    }

    static Supplier<String> dBConnectUrl = () -> "jdbc://localhost:3036/db";

    static Supplier<List<String>> dBConnectUrlList =
            () -> List.of("jdbc://localhost:3036/db", "jdbc://localhost:3037/db2");
}
