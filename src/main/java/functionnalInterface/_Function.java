package functionnalInterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {
    public static void main(String[] args) {
        final int increment = increment(0);
        System.out.println(increment);

        // Increment by One Function
        final int increment2 = incrementByOneFunction.apply(0);
        System.out.println("Imperative incrementbyOneFunction \n" +increment2);
        // Increment by One and Multiply by 10 in One Function in Imperative programming
        final int oneAndMultiplyBy10 = incrementByOneAndMultiplyBy10(0, 10);
        System.out.println("Imperative oneAndMultiplyBy10 \n"+oneAndMultiplyBy10);

        // Multiply by 10 Function
        final int multiply = multiplyBy10Function.apply(increment2);
        System.out.println(multiply);

        final Function<Integer, Integer> addOneAndthenMultiplyBy10 = incrementByOneFunction.andThen(multiplyBy10Function);
        final int result = addOneAndthenMultiplyBy10.apply(0);
        System.out.println(result);

        final int byOneandMultplyBy10 = incrementByOneAndMultiplyBy10Function.apply(0, 10);
        System.out.println(byOneandMultplyBy10);


    }

    static Function<Integer, Integer> incrementByOneFunction =
            number -> number + 1;

    static Function<Integer, Integer> multiplyBy10Function =
            number -> number * 10;

    static BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyBy10Function =
            (number, multiply) -> (number + 1)*multiply;

    static int increment(int number){
        return number+1;
    }

    static int incrementByOneAndMultiplyBy10(int number, int multiply){
        return (number+1)*multiply;
    }
}
