package functionnalInterface;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class _Consumer {
    public static void main(String[] args) {
        // Imperative customer call
        Customer jojo = new Customer("JoJo", "012333");
        System.out.println("// Imperative approach");
        greetCustomer(jojo);

        System.out.println("// Imperative approach with 2 parameters");
        greetCustomerV2(jojo,false);



        // Declarative approach -> Consumer
        System.out.println("\n// Declarative approach -> Consumer");
        customerConsumer.accept(jojo);

        // Declarative approach -> BiConsumer
        System.out.println("\n// Declarative approach -> BiConsumer");
        customerConsumerV2.accept(jojo, false);


    }
    // Imperative approach
    static void greetCustomer(Customer customer){
        System.out.println("Hello "+ customer.customerName +
                ", Thanks you for registering phone Number " + customer.phoneNumber);
    }
    // Imperative approach with BiConsumer
    static void greetCustomerV2(Customer customer, Boolean showPhoneNumber){
        System.out.println("Hello "+ customer.customerName +
                ", Thanks you for registering phone Number " + (showPhoneNumber ? customer.phoneNumber : "*******"));
    }

    // Declarative approach
    static Consumer<Customer> customerConsumer = customer -> System.out.println("Hello "+ customer.customerName +
            ", Thanks you for registering phone Number " + customer.phoneNumber);

    static BiConsumer<Customer, Boolean> customerConsumerV2 = (customer, shownumber) ->
            System.out.println("Hello "+ customer.customerName +
            ", Thanks you for registering phone Number " + (shownumber ? customer.phoneNumber : "*******"));

    static class Customer{
        private String customerName;
        private String phoneNumber;

        public Customer(String customerName, String phoneNumber) {
            this.customerName = customerName;
            this.phoneNumber = phoneNumber;
        }
    }
}
